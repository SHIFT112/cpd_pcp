
#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>


void master (int bin_count, float min_meas, float max_meas, int data_count);
void slave();

void dados(float *data, int data_count, float min_meas, float max_meas);
int find_bin(float data, float *bin_maxes, int bin_count, float min_meas);

int main(int argc, char *argv[])
{
    int pid;

    MPI_Init(&argc, &argv);
    // id do processo
    MPI_Comm_rank(MPI_COMM_WORLD, &pid);

#if DEBUG
    printf("pid: %d\n", pid);
#endif

    if (pid == 0) {

        if (argc != 5) {
            printf("usage: %s ", argv[0]);
            printf("<bin_count> <min_meas> <max_meas> <data_count>\n");
            exit(0);
        }

        // buscar argumentos
        int bin_count = strtol(argv[1], NULL, 10);
        float min_meas = strtof(argv[2], NULL);
        float max_meas = strtof(argv[3], NULL);
        int data_count = strtol(argv[4], NULL, 10);

        master(bin_count, min_meas, max_meas, data_count);
    }
    else {
        slave();
    }

    MPI_Finalize();

    return 0;
}

void master (int bin_count, float min_meas, float max_meas, int data_count)
{
    // dados gerado
    float *data = calloc(data_count, sizeof(float));

    dados(data, data_count, min_meas, max_meas);

    float *bin_maxes = calloc(data_count, sizeof(float));
    int *bin_counts = calloc(bin_count, sizeof(int));

    // largura de um balde
    float bin_width = (max_meas - min_meas) / bin_count;


    // inicializar bin_maxes - intervalos de valores de cada balde
    int b;
    for (b = 0; b < bin_count; b++) {
        bin_maxes[b] = min_meas + bin_width * (b+1);
#if DEBUG
        printf("b:%d - %f\n", b, bin_maxes[b]);
#endif
    }

    // Parallel

    int n_proc;
    MPI_Status status;

    // numero de procecos
    MPI_Comm_size(MPI_COMM_WORLD, &n_proc);


    // quantos dados vao para cada processo
    int offset = data_count / n_proc;

    int off = 0, proc;
    for (proc = 1; proc < n_proc; proc++) {
#if DEBUG
        printf("proc: %d/%d, off=%d, offset=%d\n", proc, n_proc, off, offset);
#endif
        MPI_Send(&bin_count, 1, MPI_INT, proc, 1, MPI_COMM_WORLD);
        MPI_Send(&offset, 1, MPI_INT, proc, 2, MPI_COMM_WORLD);

        MPI_Send(bin_maxes, bin_count, MPI_FLOAT, proc, 3, MPI_COMM_WORLD);
        MPI_Send(&min_meas, 1, MPI_FLOAT, proc, 4, MPI_COMM_WORLD);

        MPI_Send(&data[off], offset, MPI_FLOAT, proc, 0, MPI_COMM_WORLD);

        off += offset;
    }

    int *buf = calloc(bin_count, sizeof(int));

    for (proc = 1; proc < n_proc; proc++) {
        MPI_Recv(buf, bin_count, MPI_INT, proc, 0, MPI_COMM_WORLD, &status);
        int g;
        for (g = 0; g < bin_count; g++) {
            bin_counts[g] += buf[g];
        }
    }
    if (off > data_count) {
        off-=offset;
    }
    int bin;
    for (; off < data_count; off++) {
        bin = find_bin(data[off], bin_maxes, bin_count, min_meas);
        bin_counts[bin]++;
    }
    // print dos baldes
    float bin_min, bin_max;
    int i, j;
    for (i = 0; i < bin_count; i++) {

        // print inicio e fim de um balde
        bin_max = bin_maxes[i];
        bin_min = (i == 0) ? min_meas : bin_maxes[i-1];
        printf("%.3f-%.3f:\t", bin_min, bin_max);

        // histograma
        for (j = 0; j < bin_counts[i]; j++) {
            printf("X");
        }
        printf("\n");
    }
}


void slave()
{
    int offset;
    int bin_count;
    float min_meas;

    MPI_Status status;

    MPI_Recv(&bin_count, 1, MPI_INT, 0, 1, MPI_COMM_WORLD, &status);

    float *bin_maxes = calloc(bin_count, sizeof(float));

    MPI_Recv(&offset, 1, MPI_INT, 0, 2, MPI_COMM_WORLD, &status);

    MPI_Recv(bin_maxes, bin_count, MPI_FLOAT, 0, 3, MPI_COMM_WORLD, &status);
    MPI_Recv(&min_meas, 1, MPI_INT, 0, 4, MPI_COMM_WORLD, &status);

    float *buff = calloc(offset, sizeof(float));
    int *res = calloc(bin_count, sizeof(int));

    MPI_Recv(buff, offset, MPI_FLOAT, 0, 0, MPI_COMM_WORLD, &status);

    int i, bin;
    for (i = 0; i < offset; i++) {
        bin = find_bin(buff[i], bin_maxes, bin_count, min_meas);
        res[bin]++;
    }

    MPI_Send(res, bin_count, MPI_INT, 0, 0, MPI_COMM_WORLD);
}


void dados(float *data, int data_count, float min_meas, float max_meas)
{
    srandom(0);

    int i;
    for (i = 0; i < data_count; i++) {
        data[i] = min_meas + (max_meas - min_meas) * (random()/(float) RAND_MAX);
#if DEBUG
        printf("data: %f\n", data[i]);
#endif
    }
}

int find_bin(float data, float *bin_maxes, int bin_count, float min_meas)
{
    int i;

    if(min_meas <= data && data < bin_maxes[0]){
        return 0;
    }

    for (i = 1; i < bin_count; i++) {
        if(bin_maxes[i-1] <= data && data < bin_maxes[i]) {
            return i;
        }
    }

    printf("Data = %f doesn't belong to a bin!\n", data);
    printf("Quitting\n");
    exit(-1);
}

