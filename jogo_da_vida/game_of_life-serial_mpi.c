/***********************

Conway Game of Life

Fabio Gomes - MPI version

************************/

#include <stdio.h>

#include <stdlib.h>

#include <mpi.h>    // MPI

#define NI 600      // Linhas

#define NJ 500      // Colunas

#define NSTEPS 500  // number of time steps

int main(int argc, char *argv[]) {

	MPI_Init(&argc, &argv);

	int i, j, n, im, ip, jm, jp, ni, nj, nsum, isum, somaLocal=0, somaTotal=0,mid_rand=RAND_MAX / 2;
/*	int NI,NJ,NSTEPS;
	NI = atoi(argv[1]);
	NJ = NI;NSTEPS = NI;*/
	int **old, **new,*buf;  
	float x;
	double tempo;
	tempo = MPI_Wtime();

  	ni = NI + 2;  /* add 2 for left and right ghost cells */
	nj = NJ + 2;


    int pid, n_proc;
    MPI_Status status1,status2;
    // numero de procecos
    MPI_Comm_size(MPI_COMM_WORLD, &n_proc);	
    // id do processo
    MPI_Comm_rank(MPI_COMM_WORLD, &pid);

	

    // quantos dados vao para cada processo
	int i_offset = NI / n_proc;
	int off = 0, proc;

    // Se for o ultimo tem que se somar o resto
    if (pid+1 == n_proc)
		i_offset+= NI % n_proc;


	 // Intervalos
	 int i_inicio = pid * i_offset + 1;
	 int i_fim    = (pid + 1) * i_offset;
  

	 old = malloc((i_offset+2)*sizeof(int*));
	 new = malloc((i_offset+2)*sizeof(int*));
	 buf = malloc(nj*sizeof(int));

	 for(i=0; i<(i_offset+2); i++){
	    old[i] = calloc(nj,sizeof(int));
	    new[i] = calloc(nj,sizeof(int));}

	/* seed */
	// srand(time(0));

	/*  initialize elements of old to 0 or 1 */
	  for(i=0; i<(i_offset+2); i++){
	    for(j=0; j<nj; j++){
	      x = rand();
	      if(x>=mid_rand)
		old[i][j] = 1;
	    }
	   }  



	  int anterior = pid,proximo=pid;
	  if(anterior > 0)
		anterior --;
	  else
		anterior = n_proc-1;

	  if(proximo == n_proc-1)
		proximo = 0;
	  else
		proximo ++;
	
	
	
	  /*  time steps */
	  for(n=0; n<NSTEPS; n++){
		  
		/* left-right boundary conditions */

		for(i=1; i<=i_offset; i++){
		  old[i][0] = old[i][NJ];
		  old[i][NJ+1] = old[i][1];
		}
		
		
		/* Enviar a minha linha mais baixa
		   Para o anterior */
		MPI_Request request1,request2;
		MPI_Isend(&old[1], nj, MPI_INT, anterior, 2, MPI_COMM_WORLD,&request1);
		
		/* Enviar a minha linha mais alta
		   Para o proximo */
		MPI_Isend(&old[i_offset], nj, MPI_INT, proximo, 3, MPI_COMM_WORLD,&request1);


		/* Vou Receber a linha de cima
		   vinda do proximo */
		MPI_Recv(&buf[0], nj, MPI_INT, proximo , 2, MPI_COMM_WORLD, &status1);

		for(j = 0; j <= nj; j++)
			old[i_offset+1][j] = buf[j];

		
		/* Vou Receber a linha de baixo
		   vinda do anterior */
		MPI_Recv(&buf[0], nj, MPI_INT, anterior, 3, MPI_COMM_WORLD, &status2);
		for(j = 0; j <= nj; j++)
			old[0][j] = buf[j];


		//Iterar Ciclos
		for(i=1; i<=i_offset; i++){
		  for(j=1; j<=NJ; j++){
			im = i-1;
			ip = i+1;
			jm = j-1;
			jp = j+1;

			nsum =  old[im][jp] + old[i][jp] + old[ip][jp]
			  + old[im][j ]              + old[ip][j ] 
			  + old[im][jm] + old[i][jm] + old[ip][jm];

			switch(nsum){

			case 3:
			  new[i][j] = 1;
			  break;

			case 2:
			  new[i][j] = old[i][j];
			  break;

			default:
			  new[i][j] = 0;
			}
		  }
		}

		
		/* Feito o calculo vou gravar os dados
		   Copio os meus valores */
		for(i=1; i<=i_offset; i++)
		  for(j=1; j<=NJ; j++)
			 old[i][j] = new[i][j];
			  
		
		
	}

	for(i=1; i<=i_offset; i++)
	  for(j=1; j<=NJ; j++)
		 if(old[i][j]>0)
			somaLocal++;

	 
	 // Faco Soma de todos os que recebo
	 MPI_Reduce(&somaLocal, &somaTotal, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

	 tempo = MPI_Wtime()-tempo;
	 if(pid==0)
	   printf("\n%d\nNumber of live cells = %d\ntempo=%f segundos\n", NI,somaTotal,tempo);



	 MPI_Finalize();

  return 0;
}
