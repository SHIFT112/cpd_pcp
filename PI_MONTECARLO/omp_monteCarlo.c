/*Software Development in the UNIX Environment
Sample C Program

Example C Program to Compute PI Using A Monte Carlo Method

Source code:*/

/* Program to compute Pi using Monte Carlo methods */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <omp.h>
#define SEED 35791246

int main(int argc, char** argv){
   double pi,x,y,z;
   int niter,count,i,nthreads;

   printf("Enter the number of iterations used to estimate pi: ");
   scanf("%d",&niter);

   nthreads = omp_get_num_threads();
   
   /* initialize random numbers */
   srand(SEED);
   count=0;
   #pragma omp parallel for private(x,y) reduction(+:count) schedule(static,1)
   #pragma omp firstprivate(niter=niter/nthreads);

   for (i=0; i<niter; i++) {
      x = (double)rand()/RAND_MAX;
      y = (double)rand()/RAND_MAX;
      z = x*x+y*y;
      if (z<=1) count++;
      //printf ("sou o %d\n",omp_get_thread_num());
      }
   pi=(double)count/niter*4;
   printf("# of trials= %d , estimate of pi is %g \n",niter,pi);
}


