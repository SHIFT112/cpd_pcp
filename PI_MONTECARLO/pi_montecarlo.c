
/*Software Development in the UNIX Environment
Sample C Program

Example C Program to Compute PI Using A Monte Carlo Method

Source code:*/

/* Program to compute Pi using Monte Carlo methods */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include <mpi.h>

#define SEED 35791246


int main(int argc, char *argv[])
{
    int pid, n_proc;
    int offset, begin, end;
    int niter = 0, res;
    double pi;

    /* initialize random numbers */
    srand(SEED);

    MPI_Init(&argc, &argv);

    // id de processo
    MPI_Comm_rank(MPI_COMM_WORLD, &pid);
    // numero de procecos
    MPI_Comm_size(MPI_COMM_WORLD, &n_proc);

    if (pid == 0) {
        printf("Enter the number of iterations used to estimate pi: \n");
        scanf("%d", &niter);
    }

    MPI_Bcast(&niter, 1 , MPI_INT, 0, MPI_COMM_WORLD);

    offset = niter / n_proc;
    begin = pid * offset;
    end = begin + offset;

    double x, y, z;
    int i, count = 0;
    for (i = begin; i < end; i++) {
        x = (double) rand() / RAND_MAX;
        y = (double) rand() / RAND_MAX;
        z = x * x + y * y;

        if (z <= 1) {
            count++;
        }
    }
#if DEBUG
    printf("count: %d do id %d | pi parcial: %f | niter: %d\n", count, pid, (double) count / niter * 4, niter);
#endif

    MPI_Reduce(&count, &res, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

    if (pid == 0) {
        pi = (double) res / niter * 4;
        printf("# of trials = %d , estimate of pi is %g \n", niter, pi);
    }

    MPI_Finalize();
    return 0;
}
