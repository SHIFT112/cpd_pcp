
-bash-4.1$ ./script_val.sh
export OMP_NUM_THREADS=1
./omp_mat_vect_rand_split 1 8000000 8
==20463== Callgrind, a call-graph generating cache profiler
==20463== Copyright (C) 2002-2012, and GNU GPL'd, by Josef Weidendorfer et al.
==20463== Using Valgrind-3.8.1 and LibVEX; rerun with -h for copyright info
==20463== Command: ./omp_mat_vect_rand_split 1 8000000 8
==20463==
==20463== For interactive control, run 'callgrind_control -h'.
Elapsed time = 7.691711e+01 seconds
==20463==
==20463== Events    : Ir Dr Dw I1mr D1mr D1mw ILmr DLmr DLmw
==20463== Collected : 7110250339 2776080905 872027637 1340 8003147 9000624 1327 8002319 9000581
==20463==
==20463== I   refs:      7,110,250,339
==20463== I1  misses:            1,340
==20463== LLi misses:            1,327
==20463== I1  miss rate:           0.0%
==20463== LLi miss rate:           0.0%
==20463==
==20463== D   refs:      3,648,108,542  (2,776,080,905 rd + 872,027,637 wr)
==20463== D1  misses:       17,003,771  (    8,003,147 rd +   9,000,624 wr)
==20463== LLd misses:       17,002,900  (    8,002,319 rd +   9,000,581 wr)
==20463== D1  miss rate:           0.4% (          0.2%   +         1.0%  )
==20463== LLd miss rate:           0.4% (          0.2%   +         1.0%  )
==20463==
==20463== LL refs:          17,005,111  (    8,004,487 rd +   9,000,624 wr)
==20463== LL misses:        17,004,227  (    8,003,646 rd +   9,000,581 wr)
==20463== LL miss rate:            0.1% (          0.0%   +         1.0%  )
./omp_mat_vect_rand_split 1 8000 8000
==20856== Callgrind, a call-graph generating cache profiler
==20856== Copyright (C) 2002-2012, and GNU GPL'd, by Josef Weidendorfer et al.
==20856== Using Valgrind-3.8.1 and LibVEX; rerun with -h for copyright info
==20856== Command: ./omp_mat_vect_rand_split 1 8000 8000
==20856==
==20856== For interactive control, run 'callgrind_control -h'.
Elapsed time = 1.496184e+02 seconds
==20856==
==20856== Events    : Ir Dr Dw I1mr D1mr D1mw ILmr DLmr DLmw
==20856== Collected : 6910929782 2688312815 832147594 1342 16011150 8002622 1329 8002319 8002579
==20856==
==20856== I   refs:      6,910,929,782
==20856== I1  misses:            1,342
==20856== LLi misses:            1,329
==20856== I1  miss rate:           0.0%
==20856== LLi miss rate:           0.0%
==20856==
==20856== D   refs:      3,520,460,409  (2,688,312,815 rd + 832,147,594 wr)
==20856== D1  misses:       24,013,772  (   16,011,150 rd +   8,002,622 wr)
==20856== LLd misses:       16,004,898  (    8,002,319 rd +   8,002,579 wr)
==20856== D1  miss rate:           0.6% (          0.5%   +         0.9%  )
==20856== LLd miss rate:           0.4% (          0.2%   +         0.9%  )
==20856==
==20856== LL refs:          24,015,114  (   16,012,492 rd +   8,002,622 wr)
==20856== LL misses:        16,006,227  (    8,003,648 rd +   8,002,579 wr)
==20856== LL miss rate:            0.1% (          0.0%   +         0.9%  )
./omp_mat_vect_rand_split 1 8 8000000
==21874== Callgrind, a call-graph generating cache profiler
==21874== Copyright (C) 2002-2012, and GNU GPL'd, by Josef Weidendorfer et al.
==21874== Using Valgrind-3.8.1 and LibVEX; rerun with -h for copyright info
==21874== Command: ./omp_mat_vect_rand_split 1 8 8000000
==21874==
==21874== For interactive control, run 'callgrind_control -h'.
Elapsed time = 1.505837e+02 seconds
==21874==
==21874== Events    : Ir Dr Dw I1mr D1mr D1mw ILmr DLmr DLmw
==21874== Collected : 7389992062 2832080868 912027610 1344 16003158 9000624 1331 16002330 9000581
==21874==
==21874== I   refs:      7,389,992,062
==21874== I1  misses:            1,344
==21874== LLi misses:            1,331
==21874== I1  miss rate:           0.0%
==21874== LLi miss rate:           0.0%
==21874==
==21874== D   refs:      3,744,108,478  (2,832,080,868 rd + 912,027,610 wr)
==21874== D1  misses:       25,003,782  (   16,003,158 rd +   9,000,624 wr)
==21874== LLd misses:       25,002,911  (   16,002,330 rd +   9,000,581 wr)
==21874== D1  miss rate:           0.6% (          0.5%   +         0.9%  )
==21874== LLd miss rate:           0.6% (          0.5%   +         0.9%  )
==21874==
==21874== LL refs:          25,005,126  (   16,004,502 rd +   9,000,624 wr)
==21874== LL misses:        25,004,242  (   16,003,661 rd +   9,000,581 wr)
==21874== LL miss rate:            0.2% (          0.1%   +         0.9%  )
export OMP_NUM_THREADS=2
./omp_mat_vect_rand_split 2 8000000 8
==22914== Callgrind, a call-graph generating cache profiler
==22914== Copyright (C) 2002-2012, and GNU GPL'd, by Josef Weidendorfer et al.
==22914== Using Valgrind-3.8.1 and LibVEX; rerun with -h for copyright info
==22914== Command: ./omp_mat_vect_rand_split 2 8000000 8
==22914==
==22914== For interactive control, run 'callgrind_control -h'.
==22914==
==22914== Events    : Ir Dr Dw I1mr D1mr D1mw ILmr DLmr DLmw
==22914== Collected : 274398148 86400277 43187607 975 2706 526911 962 1881 526868
==22914==
==22914== I   refs:      274,398,148
==22914== I1  misses:            975
==22914== LLi misses:            962
==22914== I1  miss rate:         0.0%
==22914== LLi miss rate:         0.0%
==22914==
==22914== D   refs:      129,587,884  (86,400,277 rd + 43,187,607 wr)
==22914== D1  misses:        529,617  (     2,706 rd +    526,911 wr)
==22914== LLd misses:        528,749  (     1,881 rd +    526,868 wr)
==22914== D1  miss rate:         0.4% (       0.0%   +        1.2%  )
==22914== LLd miss rate:         0.4% (       0.0%   +        1.2%  )
==22914==
==22914== LL refs:           530,592  (     3,681 rd +    526,911 wr)
==22914== LL misses:         529,711  (     2,843 rd +    526,868 wr)
==22914== LL miss rate:          0.1% (       0.0%   +        1.2%  )
./script_val.sh: line 10: 22914 Terminated              valgrind --tool=callgrind --cache-sim=yes --separate-threads=yes ./omp_mat_vect_rand_split $th 8000000 8



-bash-4.1$ ./script_val.sh
export OMP_NUM_THREADS=2
./omp_mat_vect_rand_split 2 8000000 8
==22157== Callgrind, a call-graph generating cache profiler
==22157== Copyright (C) 2002-2012, and GNU GPL'd, by Josef Weidendorfer et al.
==22157== Using Valgrind-3.8.1 and LibVEX; rerun with -h for copyright info
==22157== Command: ./omp_mat_vect_rand_split 2 8000000 8
==22157==
--22157-- warning: Unknown Intel cache config value (0x63), ignoring
--22157-- warning: L3 cache found, using its data for the LL simulation.
==22157== For interactive control, run 'callgrind_control -h'.
Elapsed time = 6.492063e+01 seconds
==22157==
==22157== Events    : Ir Dr Dw I1mr D1mr D1mw ILmr DLmr DLmw
==22157== Collected : 7114471746 2776686711 872030085 1470 8003389 9000677 1455 8002520 9000632
==22157==
==22157== I   refs:      7,114,471,746
==22157== I1  misses:            1,470
==22157== LLi misses:            1,455
==22157== I1  miss rate:           0.0%
==22157== LLi miss rate:           0.0%
==22157==
==22157== D   refs:      3,648,716,796  (2,776,686,711 rd + 872,030,085 wr)
==22157== D1  misses:       17,004,066  (    8,003,389 rd +   9,000,677 wr)
==22157== LLd misses:       17,003,152  (    8,002,520 rd +   9,000,632 wr)
==22157== D1  miss rate:           0.4% (          0.2%   +         1.0%  )
==22157== LLd miss rate:           0.4% (          0.2%   +         1.0%  )
==22157==
==22157== LL refs:          17,005,536  (    8,004,859 rd +   9,000,677 wr)
==22157== LL misses:        17,004,607  (    8,003,975 rd +   9,000,632 wr)
==22157== LL miss rate:            0.1% (          0.0%   +         1.0%  )
./omp_mat_vect_rand_split 2 8000 8000
==22163== Callgrind, a call-graph generating cache profiler
==22163== Copyright (C) 2002-2012, and GNU GPL'd, by Josef Weidendorfer et al.
==22163== Using Valgrind-3.8.1 and LibVEX; rerun with -h for copyright info
==22163== Command: ./omp_mat_vect_rand_split 2 8000 8000
==22163==
--22163-- warning: Unknown Intel cache config value (0x63), ignoring
--22163-- warning: L3 cache found, using its data for the LL simulation.
==22163== For interactive control, run 'callgrind_control -h'.
Elapsed time = 6.191415e+01 seconds
==22163==
==22163== Events    : Ir Dr Dw I1mr D1mr D1mw ILmr DLmr DLmw
==22163== Collected : 6915151035 2688918576 832150018 1480 16011394 8002675 1465 8002521 8002630
==22163==
==22163== I   refs:      6,915,151,035
==22163== I1  misses:            1,480
==22163== LLi misses:            1,465
==22163== I1  miss rate:           0.0%
==22163== LLi miss rate:           0.0%
==22163==
==22163== D   refs:      3,521,068,594  (2,688,918,576 rd + 832,150,018 wr)
==22163== D1  misses:       24,014,069  (   16,011,394 rd +   8,002,675 wr)
==22163== LLd misses:       16,005,151  (    8,002,521 rd +   8,002,630 wr)
==22163== D1  miss rate:           0.6% (          0.5%   +         0.9%  )
==22163== LLd miss rate:           0.4% (          0.2%   +         0.9%  )
==22163==
==22163== LL refs:          24,015,549  (   16,012,874 rd +   8,002,675 wr)
==22163== LL misses:        16,006,616  (    8,003,986 rd +   8,002,630 wr)
==22163== LL miss rate:            0.1% (          0.0%   +         0.9%  )
./omp_mat_vect_rand_split 2 8 8000000
==22171== Callgrind, a call-graph generating cache profiler
==22171== Copyright (C) 2002-2012, and GNU GPL'd, by Josef Weidendorfer et al.
==22171== Using Valgrind-3.8.1 and LibVEX; rerun with -h for copyright info
==22171== Command: ./omp_mat_vect_rand_split 2 8 8000000
==22171==
--22171-- warning: Unknown Intel cache config value (0x63), ignoring
--22171-- warning: L3 cache found, using its data for the LL simulation.
==22171== For interactive control, run 'callgrind_control -h'.
Elapsed time = 6.215540e+01 seconds
==22171==
==22171== Events    : Ir Dr Dw I1mr D1mr D1mw ILmr DLmr DLmw
==22171== Collected : 7394213302 2832686632 912030035 1470 16003400 9000676 1455 16002532 9000631
==22171==
==22171== I   refs:      7,394,213,302
==22171== I1  misses:            1,470
==22171== LLi misses:            1,455
==22171== I1  miss rate:           0.0%
==22171== LLi miss rate:           0.0%
==22171==
==22171== D   refs:      3,744,716,667  (2,832,686,632 rd + 912,030,035 wr)
==22171== D1  misses:       25,004,076  (   16,003,400 rd +   9,000,676 wr)
==22171== LLd misses:       25,003,163  (   16,002,532 rd +   9,000,631 wr)
==22171== D1  miss rate:           0.6% (          0.5%   +         0.9%  )
==22171== LLd miss rate:           0.6% (          0.5%   +         0.9%  )
==22171==
==22171== LL refs:          25,005,546  (   16,004,870 rd +   9,000,676 wr)
==22171== LL misses:        25,004,618  (   16,003,987 rd +   9,000,631 wr)
==22171== LL miss rate:            0.2% (          0.1%   +         0.9%  )
export OMP_NUM_THREADS=4
./omp_mat_vect_rand_split 4 8000000 8
==22178== Callgrind, a call-graph generating cache profiler
==22178== Copyright (C) 2002-2012, and GNU GPL'd, by Josef Weidendorfer et al.
==22178== Using Valgrind-3.8.1 and LibVEX; rerun with -h for copyright info
==22178== Command: ./omp_mat_vect_rand_split 4 8000000 8
==22178==
--22178-- warning: Unknown Intel cache config value (0x63), ignoring
--22178-- warning: L3 cache found, using its data for the LL simulation.
==22178== For interactive control, run 'callgrind_control -h'.
Elapsed time = 6.380450e+01 seconds
==22178==
==22178== Events    : Ir Dr Dw I1mr D1mr D1mw ILmr DLmr DLmw
==22178== Collected : 7119247888 2777369196 872030625 1470 8003427 9000733 1455 8002554 9000687
==22178==
==22178== I   refs:      7,119,247,888
==22178== I1  misses:            1,470
==22178== LLi misses:            1,455
==22178== I1  miss rate:           0.0%
==22178== LLi miss rate:           0.0%
==22178==
==22178== D   refs:      3,649,399,821  (2,777,369,196 rd + 872,030,625 wr)
==22178== D1  misses:       17,004,160  (    8,003,427 rd +   9,000,733 wr)
==22178== LLd misses:       17,003,241  (    8,002,554 rd +   9,000,687 wr)
==22178== D1  miss rate:           0.4% (          0.2%   +         1.0%  )
==22178== LLd miss rate:           0.4% (          0.2%   +         1.0%  )
==22178==
==22178== LL refs:          17,005,630  (    8,004,897 rd +   9,000,733 wr)
==22178== LL misses:        17,004,696  (    8,004,009 rd +   9,000,687 wr)
==22178== LL miss rate:            0.1% (          0.0%   +         1.0%  )
./omp_mat_vect_rand_split 4 8000 8000
==22220== Callgrind, a call-graph generating cache profiler
==22220== Copyright (C) 2002-2012, and GNU GPL'd, by Josef Weidendorfer et al.
==22220== Using Valgrind-3.8.1 and LibVEX; rerun with -h for copyright info
==22220== Command: ./omp_mat_vect_rand_split 4 8000 8000
==22220==
--22220-- warning: Unknown Intel cache config value (0x63), ignoring
--22220-- warning: L3 cache found, using its data for the LL simulation.
==22220== For interactive control, run 'callgrind_control -h'.
Elapsed time = 6.096751e+01 seconds
==22220==
==22220== Events    : Ir Dr Dw I1mr D1mr D1mw ILmr DLmr DLmw
==22220== Collected : 6925219427 2690357121 832150571 1479 16010675 8002744 1464 8002567 8002696
==22220==
==22220== I   refs:      6,925,219,427
==22220== I1  misses:            1,479
==22220== LLi misses:            1,464
==22220== I1  miss rate:           0.0%
==22220== LLi miss rate:           0.0%
==22220==
==22220== D   refs:      3,522,507,692  (2,690,357,121 rd + 832,150,571 wr)
==22220== D1  misses:       24,013,419  (   16,010,675 rd +   8,002,744 wr)
==22220== LLd misses:       16,005,263  (    8,002,567 rd +   8,002,696 wr)
==22220== D1  miss rate:           0.6% (          0.5%   +         0.9%  )
==22220== LLd miss rate:           0.4% (          0.2%   +         0.9%  )
==22220==
==22220== LL refs:          24,014,898  (   16,012,154 rd +   8,002,744 wr)
==22220== LL misses:        16,006,727  (    8,004,031 rd +   8,002,696 wr)
==22220== LL miss rate:            0.1% (          0.0%   +         0.9%  )
./omp_mat_vect_rand_split 4 8 8000000
==22227== Callgrind, a call-graph generating cache profiler
==22227== Copyright (C) 2002-2012, and GNU GPL'd, by Josef Weidendorfer et al.
==22227== Using Valgrind-3.8.1 and LibVEX; rerun with -h for copyright info
==22227== Command: ./omp_mat_vect_rand_split 4 8 8000000
==22227==
--22227-- warning: Unknown Intel cache config value (0x63), ignoring
--22227-- warning: L3 cache found, using its data for the LL simulation.
==22227== For interactive control, run 'callgrind_control -h'.
Elapsed time = 6.030627e+01 seconds
==22227==
==22227== Events    : Ir Dr Dw I1mr D1mr D1mw ILmr DLmr DLmw
==22227== Collected : 7403328404 2833988995 912030593 1471 16003464 9000742 1456 15927521 9000697
==22227==
==22227== I   refs:      7,403,328,404
==22227== I1  misses:            1,471
==22227== LLi misses:            1,456
==22227== I1  miss rate:           0.0%
==22227== LLi miss rate:           0.0%
==22227==
==22227== D   refs:      3,746,019,588  (2,833,988,995 rd + 912,030,593 wr)
==22227== D1  misses:       25,004,206  (   16,003,464 rd +   9,000,742 wr)
==22227== LLd misses:       24,928,218  (   15,927,521 rd +   9,000,697 wr)
==22227== D1  miss rate:           0.6% (          0.5%   +         0.9%  )
==22227== LLd miss rate:           0.6% (          0.5%   +         0.9%  )
==22227==
==22227== LL refs:          25,005,677  (   16,004,935 rd +   9,000,742 wr)
==22227== LL misses:        24,929,674  (   15,928,977 rd +   9,000,697 wr)
==22227== LL miss rate:            0.2% (          0.1%   +         0.9%  )




-bash-4.1$ ./script_val.sh
export OMP_NUM_THREADS=1
./omp_mat_vect_rand_split 1 8000000 8
==22839== Callgrind, a call-graph generating cache profiler
==22839== Copyright (C) 2002-2012, and GNU GPL'd, by Josef Weidendorfer et al.
==22839== Using Valgrind-3.8.1 and LibVEX; rerun with -h for copyright info
==22839== Command: ./omp_mat_vect_rand_split 1 8000000 8
==22839==
--22839-- warning: Unknown Intel cache config value (0x63), ignoring
--22839-- warning: L3 cache found, using its data for the LL simulation.
==22839== For interactive control, run 'callgrind_control -h'.
Elapsed time = 6.248750e+01 seconds
==22839==
==22839== Events    : Ir Dr Dw I1mr D1mr D1mw ILmr DLmr DLmw
==22839== Collected : 7110261091 2776083559 872028626 1357 8003222 9000625 1342 8002357 9000580
==22839==
==22839== I   refs:      7,110,261,091
==22839== I1  misses:            1,357
==22839== LLi misses:            1,342
==22839== I1  miss rate:           0.0%
==22839== LLi miss rate:           0.0%
==22839==
==22839== D   refs:      3,648,112,185  (2,776,083,559 rd + 872,028,626 wr)
==22839== D1  misses:       17,003,847  (    8,003,222 rd +   9,000,625 wr)
==22839== LLd misses:       17,002,937  (    8,002,357 rd +   9,000,580 wr)
==22839== D1  miss rate:           0.4% (          0.2%   +         1.0%  )
==22839== LLd miss rate:           0.4% (          0.2%   +         1.0%  )
==22839==
==22839== LL refs:          17,005,204  (    8,004,579 rd +   9,000,625 wr)
==22839== LL misses:        17,004,279  (    8,003,699 rd +   9,000,580 wr)
==22839== LL miss rate:            0.1% (          0.0%   +         1.0%  )







-bash-4.1$ !ti
time  ./omp_mat_vect_rand_split 1 8000000 8
Elapsed time = 4.431012e-01 seconds

real    0m1.294s
user    0m1.151s
sys     0m0.137s
-bash-4.1$ time  ./omp_mat_vect_rand_split 1 8000 8000
Elapsed time = 3.104360e-01 seconds

real    0m0.980s
user    0m0.862s
sys     0m0.110s
-bash-4.1$ time  ./omp_mat_vect_rand_split 1 8 8000000
Elapsed time = 4.125731e-01 seconds

real    0m1.329s
user    0m1.181s
sys     0m0.140s
